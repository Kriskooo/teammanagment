createuser => string name

createTeam => string team

createstory => string userName, string userName, string teamName, string boardName, string title, string description*, string priority, string size(LARGE, MEDIUM, SMALL), string status.

createfeedback => string userName, string teamName, string boardName, string title, string description, int rating ,string feedbackstatus

createbug => string userName, string teamName, string boardName, string title, string description, string priority, string severity, string bugStatus

showuseractivity => string username


showteamactivity => string teamname


showboardactivity => string teamName, string board

showallusers =>

showallteammembers => string teamName

showallteamboards => string teamName

listallteams => 

changestatusofstory => string story

changestatusoffeedback => string feedback, string status

changestatusofbug => string bug, string status

changesizeofstory => string story, string size

changeseverityofbug => string bug, string severity

changeratingoffeedback => string feedback, int rating

changepriorityofstory => string story, string priority

changepriorityofbug => string bug, string priority

createboardinteam => string team, string board

addusertoteam => string userName, string teamName

addcommenttotask => string comment, string author, string workItemName