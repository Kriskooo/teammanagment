﻿namespace TaskManagement.Contracts
{
    public interface IComment
    {
        string Content { get; }

        string Author { get; }
    }
}
