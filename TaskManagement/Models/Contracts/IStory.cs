﻿using TaskManagement.Enums;

namespace TaskManagement.Contracts
{
    public interface IStory : IWorkItem
    {
        Priority Priority { get; set; }

        Size Size { get; set; }

        StoryStatus Status { get; set; }

        IUser Assignee { get; set; }
    }
}
