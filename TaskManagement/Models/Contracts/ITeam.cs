﻿using System.Collections.Generic;

namespace TaskManagement.Contracts
{
    public interface ITeam
    {
        public string Name { get; }

        IList<IUser> Users { get; }

        IList<IBoard> Boards { get; }

        public void AddBoard(IBoard board);

        public void AddUser(IUser user);
    }
}
