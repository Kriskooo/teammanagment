﻿using System.Collections.Generic;

namespace TaskManagement.Contracts
{
    public interface IUser
    {
        public string Name { get; }

        IList<IWorkItem> WorkItems { get; }

        public void AddWorkItem(IWorkItem item);

        public void RemoveWorkItem(IWorkItem item);
    }
}
