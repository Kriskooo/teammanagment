﻿using TaskManagement.Enums;

namespace TaskManagement.Contracts
{
    public interface IFeedback : IWorkItem
    {
        int Rating { get; set; }

        FeedbackStatus Status { get; set; }
    }
}
