﻿using System.Collections.Generic;
using TaskManagement.Enums;

namespace TaskManagement.Contracts
{
    public interface IBug : IWorkItem
    {
        IList<string> StepsToReproduce { get; }

        Priority Priority { get; set; }

        Severity Severity { get; set; }

        BugStatus Status { get; set; }

        IUser Assignee { get; set; }
    }
}
