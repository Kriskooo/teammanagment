﻿using System.Collections.Generic;

namespace TaskManagement.Contracts
{
    public interface IBoard
    {
        public string Name { get; }

        IList<IWorkItem> WorkItems { get; }

        public void AddWorkItem(IWorkItem workItem);
    }
}
