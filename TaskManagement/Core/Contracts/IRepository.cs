﻿using System.Collections.Generic;
using TaskManagement.Contracts;

namespace TaskManagement.Core.Contracts
{
    public interface IRepository
    {
        IList<ITeam> Teams { get; }

        IList<IUser> Users { get; }

        IList<IWorkItem> WorkItems { get; }

        IList<IActivityHistory> ActivityHistories { get; }
        bool UserExists(string userName);

        bool TeamExists(string teamName);

        IUser CreateUser(string userName);

        ITeam CreateTeam(string teamName);

        IActivityHistory CreateActivityHistory(IUser user, ITeam team, IBoard board, IWorkItem workItem);

        IUser GetUserByName(string userName);

        ITeam GetTeamByName(string teamName);

        IList<IActivityHistory> ShowUserActivity(string userName);

        IList<IActivityHistory> ShowTeamActivity(string teamName);

        public void AddWorkItem(IWorkItem workItem);
    }
}
