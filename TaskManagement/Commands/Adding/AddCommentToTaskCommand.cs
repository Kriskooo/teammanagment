﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagement.Core.Contracts;

namespace TaskManagement.Commands.Adding
{
    public class AddCommentToTaskCommand : BaseCommand
    {
        private readonly IRepository repository;
        private readonly IList<string> commandParameters;

        public AddCommentToTaskCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public override string Execute()
        {
            if (this.commandParameters.Count < 3)
            {
                throw new ArgumentException("please provide comment and author");
            }

            string comment = this.commandParameters[0];
            string author = this.commandParameters[1];
            string workItemName = this.commandParameters[2];

            this.repository.WorkItems.FirstOrDefault(x => x.Title.ToLower() == workItemName.ToLower()).AddComment(comment, author);

            return $"Added comment to {workItemName}";
        }
    }
}
