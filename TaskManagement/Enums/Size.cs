﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagement.Enums
{
    public enum Size
    {
        LARGE,
        MEDIUM,
        SMALL
    }
}
