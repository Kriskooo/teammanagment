﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagement.Enums
{
    public enum StoryStatus
    {
        NOTDONE,
        INPROGRESS,
        DONE
    }
}
