﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagement.Enums
{
    public enum FeedbackStatus
    {
        NEW,
        UNSCHEDULED,
        SCHEDULED,
        DONE
    }
}
