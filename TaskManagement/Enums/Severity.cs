﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagement.Enums
{
    public enum Severity
    {
        CRITICAL,
        MAJOR,
        MINOR
    }
}
