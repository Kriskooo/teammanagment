﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagement.Common
{
    public interface IConsoleActions
    {
        public string ReadingLine();

        public void Write(string input);
    }
}
