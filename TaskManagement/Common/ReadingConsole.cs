﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagement.Common
{
    public class ReadingConsole : IConsoleActions
    {
        public string ReadingLine()
        {
            return Console.ReadLine();
        }

        public void Write(string input)
        {
            Console.Write(input);
        }
    }
}
