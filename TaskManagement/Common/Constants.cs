﻿namespace TaskManagement.Common
{
    public class Constants
    {
        public const int TITLE_NAME_LEN_MIN = 10;
        public const int TITLE_NAME_LEN_MAX = 50;
        public const string TITLE_NULL_ERR = "Title must not be NULL!";
        public static string TITLE_NAME_LEN_ERR = $"Title must be between {TITLE_NAME_LEN_MIN} and {TITLE_NAME_LEN_MAX} characters long!";

        public const int DESCRIPTION_LEN_MIN = 10;
        public const int DESCRIPTION_LEN_MAX = 500;
        public const string DESCRIPTION_NULL_ERR = "Description must not be NULL!";
        public static string DESCRIPTION_LEN_ERR = $"Description must be between {DESCRIPTION_LEN_MIN} and {DESCRIPTION_LEN_MAX} characters long!";

        public const int TEAM_NAME_LEN_MIN = 5;
        public const int TEAM_NAME_LEN_MAX = 15;
        public const string TEAM_NULL_ERR = "Team must not be NULL!";
        public static string TEAM_NAME_LEN_ERR = $"Team must be between {TEAM_NAME_LEN_MIN} and {TEAM_NAME_LEN_MAX} symbols.";

        public const int USER_NAME_LEN_MIN = 5;
        public const int USER_NAME_LEN_MAX = 15;
        public const string USER_NULL_ERR = "User must not be NULL!";
        public static string USER_NAME_LEN_ERR = $"User must be between {USER_NAME_LEN_MIN} and {USER_NAME_LEN_MAX} symbols.";

        public const int BOARD_NAME_LEN_MIN = 5;
        public const int BOARD_NAME_LEN_MAX = 10;
        public const string BOARD_NULL_ERR = "Board must not be NULL!";
        public static string BOARD_NAME_LEN_ERR = $"Board must be between {BOARD_NAME_LEN_MIN} and {BOARD_NAME_LEN_MAX} symbols.";

        public const int CREATE_USER_MIN_PARAMETERS = 1;
        public const string CREATE_NULL_ERR = "CreateUser must not be NULL!";
        public const string CREATE_USER_LEN_ERR = "Invalid number of arguments. Expected: {0} Received: {1}";

        public const int CREATE_TEAM_MIN_PARAMETERS = 1;
        public const string CREATE_TEAM_NULL_ERR = "CreateTeam must not be NULL!";
        public const string CREATE_TEAM_LEN_ERR = "Invalid number of arguments. Expected: {0} Received: {1}";

        public const int ADD_USER_MIN_PARAMETERS = 2;
        public const string ADD_NULL_ERR = "CreateUser must not be NULL!";
        public const string ADD_USER_LEN_ERR = "Invalid number of arguments. Expected: {0} Received: {1}";

        public const int RATING_MINIMUM_VALUE = 0;
        public const int RATING_MAXIMUM_VALUE = 5;
        public static string RATING_VALUE_ERR = $"Rating must be between {RATING_MINIMUM_VALUE} and {RATING_MAXIMUM_VALUE}.";
    }
}
