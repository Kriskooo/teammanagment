﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Contracts;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class TeamClassTests
    {
        [TestMethod]
        public void PassValues_Should()
        {
            //Act
            string team = "Killers";
            var teamName = new Team(team);

            //Assert
            Assert.AreEqual(teamName.Name, team);
        }

        [TestMethod]
        public void Constructor_ShouldCreate_WhenValueCorrect()
        {
            var addBoard = "Killers";
            var board = new Team(addBoard);
            var boards = new Mock<IBoard>();
            board.AddBoard(boards.Object);
            var expected = new List<IBoard> { boards.Object };
            var actual = board.Boards;

            Assert.AreEqual(actual.Count, expected.Count);
        }

        [TestMethod]
        public void TestIUSER_WhenValueCorrect()
        {
            var nameUser = "TestUser";
            var teamName = "TestTeam";
            var team = new Team(teamName);
            var users = new Mock<IUser>();
            users.SetupGet(x => x.Name).Returns(nameUser);
            team.AddUser(users.Object);
            var expected = new List<IUser> { users.Object };
            var actual = team.Users;

            Assert.AreEqual(actual[0].Name, expected[0].Name);
        }
    }
}
