﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Commands;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ShowAllTeamsCommandTests
    {
        [TestMethod]
        public void Should_PrintTeams()
        {
            var teamName = "TestTeam";
            var repository = new Mock<IRepository>();
            var team = new Mock<ITeam>();
            var commandParameters = new List<string>();
            team.SetupGet(x => x.Name).Returns(teamName);
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            var test = new ShowAllTeamsCommand(commandParameters, repository.Object);
            var sb = new StringBuilder();
            sb.AppendLine("===All Teams===");
            foreach (var item in repository.Object.Teams)
            {
                sb.AppendLine($"Team: {item.Name}");
            }

            var expected = sb.ToString().Trim();
            var actual = test.Execute();
            Assert.AreEqual(expected, actual);
        }
    }
}
