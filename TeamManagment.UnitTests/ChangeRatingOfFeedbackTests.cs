﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TaskManagement.Commands;
using TaskManagement.Commands.Creating;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ChangeRatingOfFeedbackTests
    {
        [TestMethod]
        public void Execute_Should_ChangePriority()
        {
            var itemName = "Test Title";
            var rating = "5";            
            var commandParameters = new List<string> { itemName, rating };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IFeedback>();
            testItem.Setup(x => x.Rating).Returns(int.Parse(rating));
            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { testItem.Object });
            var test = new ChangeRatingOfFeedback(commandParameters, repository.Object);
            var previosRating = testItem.Object.Rating;
            var actual = test.Execute();            
            var expected = $"Changed rating of Feedback {itemName} with ID: {testItem.Object.Id} from {previosRating} to {rating}";
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void Execute_ShouldThrow_NotEnoughParameters()
        {
            var itemName = "Test Title";
            var commandParameters = new List<string>();
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IFeedback>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { testItem.Object });
            var test = new ChangeRatingOfFeedback(commandParameters, repository.Object);

            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfWorkItemDoesNotExists()
        {
            var itemName = "Test Title";
            var rating = "5";
            
            var commandParameters = new List<string> { itemName, rating };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IFeedback>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem>());
            var test = new ChangeRatingOfFeedback(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }
    }
}
