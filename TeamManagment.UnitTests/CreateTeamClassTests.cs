﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Commands.Creating;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class CreateTeamClassTests
    {
        [TestMethod]
        public void ShouldThrow_IfTeamExists()
        {
            var team = "CodeKillers";
            var commandParameters = new List<string> { team };
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(team)).Returns(true);
            var test = new CreateTeamCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void ShouldCreate_IfTeamDontExists()
        {
            var team = "CodeKillers";
            var commandParameters = new List<string> { team };
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(team)).Returns(false);
            var test = new CreateTeamCommand(commandParameters, repository.Object);
            var actual = test.Execute();
            var expected = $"Succesfully Created Team with name {team}";
            Assert.AreEqual(expected, actual);
        }
    }
}