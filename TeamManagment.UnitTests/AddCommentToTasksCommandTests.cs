﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Commands.Adding;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class AddCommentToTasksCommandTests
    {
        [TestMethod]
        public void Should_Throw_WhenParametersLessThanExpected()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();
            var test = new AddCommentToTaskCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_AddComment()
        {
            var bugName = "TestBug";
            var author = "TestUser";
            var comment = "TestComment";
            var commandParameters = new List<string> { comment, author, bugName };
            var bug = new Mock<IBug>();
            bug.SetupGet(x => x.Title).Returns(bugName);
            bug.SetupGet(x => x.Comment).Returns(new List<IComment>());
            var repository = new Mock<IRepository>();
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { bug.Object });
            var test = new AddCommentToTaskCommand(commandParameters, repository.Object);

            var expected = $"Added comment to {bugName}";
            var actual = test.Execute();

            Assert.AreEqual(expected, actual);
        }
    }
}
