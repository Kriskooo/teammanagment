﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Commands;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ShowBoardActivityCommandTests
    {
        [TestMethod]
        public void Should_Throw_WhenParametersLessThanExpected()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();
            var test = new ShowBoardActivityCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_Throw_WhenTeamDoesNotExists()
        {
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var commandParameters = new List<string> { teamName, boardName};
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(teamName)).Returns(false);
            var test = new ShowBoardActivityCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_Throw_WhenBoardDoesNotExists()
        {
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var bugName = "TestBug";
            var commandParameters = new List<string> { teamName, boardName };

            var bug = new Mock<IBug>();
            bug.SetupGet(x => x.Title).Returns(bugName);

            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns("FAKEBOARD");
            board.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { bug.Object });

            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            team.SetupGet(x => x.Boards).Returns(new List<IBoard> { board.Object });

            var activity = new Mock<IActivityHistory>();
            activity.SetupGet(x => x.Team).Returns(team.Object);
            activity.SetupGet(x => x.Board).Returns(board.Object);

            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });            
            repository.SetupGet(x => x.ActivityHistories).Returns(new List<IActivityHistory> { activity.Object });

            var test = new ShowBoardActivityCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_Print()
        {
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var bugName = "TestBug";
            var commandParameters = new List<string> { teamName, boardName };

            var bug = new Mock<IBug>();
            bug.SetupGet(x => x.Title).Returns(bugName);

            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns(boardName);
            board.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { bug.Object });

            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            team.SetupGet(x => x.Boards).Returns(new List<IBoard> { board.Object });

            var activity = new Mock<IActivityHistory>();
            activity.SetupGet(x => x.Team).Returns(team.Object);
            activity.SetupGet(x => x.Board).Returns(board.Object);

            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.SetupGet(x => x.ActivityHistories).Returns(new List<IActivityHistory> { activity.Object });
            
            var boardActivity = repository.Object
                .ActivityHistories.FirstOrDefault(x => x.Team.Name.ToLower() == teamName.ToLower() && x.Board.Name.ToLower() == boardName.ToLower())
                .Board.WorkItems.ToList();
            var test = new ShowBoardActivityCommand(commandParameters, repository.Object);
            var actual = test.Execute();
            var sb = new StringBuilder();
            sb.AppendLine($"Board: {boardName} in team: {teamName} has following workItems.");

            foreach (var item in boardActivity)
            {
                sb.AppendLine($"Title: {item.Title}");
                sb.AppendLine($"Description: {item.Description}");
                sb.AppendLine("  ---------  ");
            }
           
            var expected = sb.ToString().TrimEnd();
            Assert.AreEqual(actual, expected);
        }
    }
}
