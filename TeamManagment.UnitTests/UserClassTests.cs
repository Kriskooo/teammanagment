﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class UserClassTests
    {
        [TestMethod]
        public void PassValues_Should()
        {
            //Act
            string name = "Krisko";
            var user = new User(name);

            //Assert
            Assert.AreEqual(user.Name, name);
        }

        [TestMethod]        
        public void ThrowWhenUsername_IsShorter_Should()
        {
            //Act
            string name = "Kri";
            Assert.ThrowsException<ArgumentException>(() => new User(name));
        }
                
        [TestMethod]
        public void Constructor_ShouldCreate_WhenValueCorrect()
        {
            var name = "TestUser";
            var user = new User(name);
            var workItem = new Mock<IWorkItem>();
            user.AddWorkItem(workItem.Object);
            var expected = new List<IWorkItem> { workItem.Object };
            var actual = user.WorkItems;

            Assert.AreEqual(actual.Count, expected.Count);
        }

        [TestMethod]
        public void RemoveWorkItem_ShouldRemove_WhenValueCorrect()
        {
            var name = "TestUser";
            var user = new User(name);
            var workItem = new Mock<IWorkItem>();
            user.AddWorkItem(workItem.Object);
            user.RemoveWorkItem(workItem.Object);
            var expected = new List<IWorkItem>();
            var actual = user.WorkItems;

            Assert.AreEqual(actual.Count, expected.Count);
        }
    }
}
