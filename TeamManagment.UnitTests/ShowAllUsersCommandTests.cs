﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Commands;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ShowAllUsersCommandTests
    {
        [TestMethod]
        public void Should_PrintUsers()
        {
            var userName = "TestUser";
            var repository = new Mock<IRepository>();
            var user = new Mock<IUser>();
            var commandParameters = new List<string>();
            user.SetupGet(x => x.Name).Returns(userName);
            repository.SetupGet(x => x.Users).Returns(new List<IUser> { user.Object });
            var test = new ShowAllUsersCommand(commandParameters, repository.Object);
            var sb = new StringBuilder();
            sb.AppendLine("===ShowAllUsers===");

            foreach (var item in repository.Object.Users)
            {
                sb.AppendLine($"User: {item.Name}");
            }

            var expected = sb.ToString().Trim();
            var actual = test.Execute();
            Assert.AreEqual(expected, actual);
        }
    }
}
