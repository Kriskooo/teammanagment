﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Models;
using Moq;
using TaskManagement.Core.Contracts;
using TaskManagement.Commands.Creating;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class CreateUserClassTests
    {
        [TestMethod]
        public void ShouldThrow_IfUserExists()
        {
            var userName = "Krisko";
            var commandParameters = new List<string> { userName };
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.UserExists(userName)).Returns(true);
            var test = new CreateUserCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void ShouldCreate_IfUserDontExists()
        {
            var userName = "Krisko";
            var commandParameters = new List<string> { userName };
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.UserExists(userName)).Returns(false);
            var test = new CreateUserCommand(commandParameters, repository.Object);
            var actual = test.Execute();
            var expected = $"Succesfully Created User with name {userName}";
            Assert.AreEqual(expected, actual);
        }
    }
}