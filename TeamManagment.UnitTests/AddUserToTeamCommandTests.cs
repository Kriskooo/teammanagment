﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Commands.Adding;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class AddUserToTeamCommandTests
    {
        [TestMethod]
        public void Should_Throw_WhenParametersLessThanExpected()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();
            var test = new AddUserToTeamCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_Throw_WhenTeamDoesNotExist()
        {
            var userName = "TestUser";
            var teamName = "TestTeam";
            
            var commandParameters = new List<string> { userName, teamName };
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.UserExists(userName)).Returns(true);
            repository.Setup(x => x.TeamExists(teamName)).Returns(false);
            var test = new AddUserToTeamCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_Throw_WhenUserDoesNotExist()
        {
            var userName = "TestUser";
            var teamName = "TestTeam";
            
            var commandParameters = new List<string> { userName, teamName };
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.UserExists(userName)).Returns(false);
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);
            var test = new AddUserToTeamCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Should_AddUser()
        {
            var userName = "TestUser";
            var teamName = "TestTeam";
            var user = new Mock<IUser>();
            user.SetupGet(x => x.Name).Returns(userName);

            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
                                    
            var commandParameters = new List<string> { userName, teamName };
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.UserExists(userName)).Returns(true);
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);
            repository.Setup(x => x.GetTeamByName(teamName)).Returns(team.Object);
            repository.Setup(x => x.GetUserByName(userName)).Returns(user.Object);
            var test = new AddUserToTeamCommand(commandParameters, repository.Object);
            var expected = $"User {userName} was added to Team {teamName}.";
            var actual = test.Execute();
            
            Assert.AreEqual(actual, expected);
        }
    }
}
