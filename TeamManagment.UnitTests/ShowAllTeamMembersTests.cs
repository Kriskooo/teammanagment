﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Commands;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ShowAllTeamMembersTests
    {
        [TestMethod]
        public void ShouldThrowAll_TeamExists()
        {
            var teamName = "SuicideSquad";
            var commandParameters = new List<string> { teamName };
            var repository = new Mock<IRepository>();
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            repository.Setup(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.TeamExists(teamName)).Returns(false);
            var test = new ShowAllTeamMembersCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void ShouldAll_ParametersLess_ThenExpected()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();

            var test = new ShowAllTeamMembersCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }


        [TestMethod]
        public void ShouldAll_Board_ExpectedPrinted()
        {
            var teamTest = "TeamKillers";
            var testMember = "TestMember";

            var repository = new Mock<IRepository>();
            var commandParameters = new List<string> { teamTest, testMember };

            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamTest);
            var user = new Mock<IUser>();
            user.SetupGet(x => x.Name).Returns(teamTest);
            team.SetupGet(x => x.Users).Returns(new List<IUser> { user.Object });
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.UserExists(teamTest)).Returns(true);
            var test = new ShowAllTeamMembersCommand(commandParameters, repository.Object);
            var sb = new StringBuilder();
            sb.AppendLine($"===ShowAllMembersOfTeam: {teamTest}===");

            foreach (var item in team.Object.Users)
            {
                sb.AppendLine($"members: {item.Name}");
            }
            sb.AppendLine($" ----------  ");
            var expected = sb.ToString().Trim();
            var actual = test.Execute();
            Assert.AreEqual(expected, actual);
        }
    }
}