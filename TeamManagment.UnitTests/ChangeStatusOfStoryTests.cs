﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TaskManagement.Commands;
using TaskManagement.Commands.Creating;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ChangeStatusOfStoryTests
    {
        [TestMethod]
        public void Execute_Should_ChangePriority()
        {
            var itemName = "Test Title";
            var priority = "DONE";
            var previosPriority = StoryStatus.NOTDONE;
            var commandParameters = new List<string> { itemName, priority };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IStory>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { testItem.Object });
            var test = new ChangeStatusOfStory(commandParameters, repository.Object);
            var actual = test.Execute();
            var expected = $"Changed status of Story {itemName} with ID: {testItem.Object.Id} from {previosPriority} to {priority}";
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfWorkItemDoesNotExists()
        {
            var itemName = "Test Title";
            var priority = "DONE";

            var commandParameters = new List<string> { itemName, priority };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IStory>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem>());
            var test = new ChangeStatusOfStory(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfLessParameters()
        {
            var itemName = "Test Title";           

            var commandParameters = new List<string>();
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IStory>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem>());
            var test = new ChangeStatusOfStory(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }
    }
}
