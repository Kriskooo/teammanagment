﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement;
using TaskManagement.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ActivityHistoryClassTests
    {
        [TestMethod]
        public void PassValues_Should()
        {
            //Arrange
            var userName = "UserTest";
            var teamName = "TeamName";
            var boardName = "BoardTest";
            var item = "ItemTest";
            var user = new Mock<IUser>();
            user.SetupGet(x => x.Name).Returns(userName);
            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns(boardName);
            var workItem = new Mock<IBug>();
            workItem.SetupGet(x => x.Title).Returns(item);
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);



            //Act
            var activityHistory = new ActivityHistory(user.Object, team.Object, board.Object, workItem.Object);


            //Assert
            Assert.AreEqual(activityHistory.User.Name, userName);
            Assert.AreEqual(activityHistory.Team.Name, teamName);
            Assert.AreEqual(activityHistory.Board.Name, boardName);
            Assert.AreEqual(activityHistory.WorkItem.Title, item);

        }
    }
}
