﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class FeedbackClassTests
    {
        [TestMethod]
        public void PassValues_Should()
        {
            //Arrange
            var id = 1;
            var title = "Test Titleee";
            var description = "Test Description";
            var rating = 5;

            //Act
            var feedback = new Feedback(id, title, description, rating, FeedbackStatus.NEW);

            //Assert
            Assert.AreEqual(feedback.Title, "Test Titleee");
            Assert.AreEqual(feedback.Description, "Test Description");
            Assert.AreEqual(feedback.Rating, 5);
            Assert.AreEqual(feedback.Status, FeedbackStatus.NEW);
        }
    }
}
