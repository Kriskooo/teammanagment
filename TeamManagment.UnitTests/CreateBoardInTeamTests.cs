﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskManagement.Commands;
using Moq;
using TaskManagement.Core.Contracts;
using TaskManagement.Core;
using TaskManagement.Models;
using TaskManagement.Contracts;
using System.Linq;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class CreateBoardInTeamTests
    {
        [TestMethod]
        public void Execute_ShouldThrow_IfParametersLessThanExpected()
        {
            var repository = new Mock<IRepository>();            
            var commandParameters = new List<string>();

            var test = new CreateBoardInTeamCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfTeamDoesNotExist()
        {
            var testTeam = "TestTeam";
            var repository = new Mock<IRepository>();            
            repository.Setup(x => x.TeamExists(testTeam)).Returns(false);
            var commandParameters = new List<string> { testTeam, "TestBoard" };
            var test = new CreateBoardInTeamCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfBoardDoesExists()
        {
            var testTeam = "TestTeam";
            var testBoard = "TestBoard";
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string> { testTeam, testBoard };
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(testTeam);
            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns(testTeam);
            team.SetupGet(x => x.Boards).Returns(new List<IBoard> { board.Object });
            repository.Setup(x => x.Teams.Contains(team.Object)).Returns(true);
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.TeamExists(testTeam)).Returns(true);
            //team.SetupProperty(x => x.Boards, new List<IBoard> { board.Object });
            var test = new CreateBoardInTeamCommand(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldAdd_IfBoardDoesNotExists()
        {
            var testTeam = "TestTeam";
            var testBoard = "TestBoard";
            var boardToAdd = "BoardToAdd";
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string> { testTeam, boardToAdd };
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(testTeam);
            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns(testBoard);
            team.SetupGet(x => x.Boards).Returns(new List<IBoard>());
            
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.TeamExists(testTeam)).Returns(true);
            repository.Setup(x => x.GetTeamByName(testTeam)).Returns(team.Object);
            var test = new CreateBoardInTeamCommand(commandParameters, repository.Object);
            var expected = $"Succesfully Created Board with name {boardToAdd} in team {testTeam}";
            var actual = test.Execute();
            Assert.AreEqual(expected, actual);
        }
    }
}
