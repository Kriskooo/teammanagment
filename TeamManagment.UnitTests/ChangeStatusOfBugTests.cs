﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TaskManagement.Commands;
using TaskManagement.Commands.Creating;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ChangeStatusOfBugTests
    {
        [TestMethod]
        public void Execute_Should_ChangePriority()
        {
            var itemName = "Test Title";
            var status = "ACTIVE";
            var previosPriority = BugStatus.ACTIVE;
            var commandParameters = new List<string> { itemName, status };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IBug>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { testItem.Object });
            var test = new ChangeStatusOfBug(commandParameters, repository.Object);
            var actual = test.Execute();
            var expected = $"Changed status of Bug {itemName} with ID: {testItem.Object.Id} from {previosPriority} to {status}";
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfWorkItemDoesNotExists()
        {
            var itemName = "Test Title";
            var priority = "ACTIVE";

            var commandParameters = new List<string> { itemName, priority };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IStory>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem>());
            var test = new ChangeStatusOfBug(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }
    }
}
