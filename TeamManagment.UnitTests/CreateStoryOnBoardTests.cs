﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using TaskManagement.Commands.Creating;
using TaskManagement.Common;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class CreateStoryOnBoardTests
    {
        [TestMethod]
        public void Execute_ShouldThrow_IfParametersLessThanExpected()
        {
            var id = 1;
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();

            var test = new CreateStoryOnBoard(id, commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfTeamDoesNotExist()
        {
            var id = 1;
            var userName = "TestUser";
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var title = "Test Title";
            var description = "TestDescription";
            var priority = "HIGH";
            var size = "LARGE";
            var status = "NOTDONE";
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(teamName)).Returns(false);
            var commandParameters = new List<string> { userName, teamName, boardName, title, description, priority, size, status };
            var test = new CreateStoryOnBoard(id, commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfBoardDoesNotExist()
        {
            var id = 1;
            var userName = "TestUser";
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var title = "Test Title";
            var description = "TestDescription";
            var priority = "HIGH";
            var size = "LARGE";
            var status = "NOTDONE";
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);
            var commandParameters = new List<string> { userName, teamName, boardName, title, description, priority, size, status };
            var team = new Mock<ITeam>();
            team.SetupGet(x => x.Name).Returns(teamName);
            var board = new Mock<IBoard>();
            board.SetupGet(x => x.Name).Returns(teamName);
            team.SetupGet(x => x.Boards).Returns(new List<IBoard> { board.Object });
            repository.Setup(x => x.Teams.Contains(team.Object)).Returns(true);
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);

            var test = new CreateStoryOnBoard(id, commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }

        [TestMethod]
        public void Execute_ShouldCreate_IfCorrectParameters()
        {
            var id = 1;
            var userName = "TestUser";
            var teamName = "TestTeam";
            var boardName = "TestBoard";
            var title = "Test Title";
            var description = "TestDescription";
            var priority = "HIGH";
            var size = "LARGE";
            var status = "NOTDONE";
            var repository = new Mock<IRepository>();
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);
            var commandParameters = new List<string> { userName, teamName, boardName, title, description, priority, size, status };
            var team = new Mock<ITeam>();
            var user = new Mock<IUser>();
            var board = new Mock<IBoard>();
            user.SetupGet(x => x.Name).Returns(userName);
            team.SetupGet(x => x.Name).Returns(teamName);
            board.SetupGet(x => x.Name).Returns(boardName);

            team.SetupGet(x => x.Boards).Returns(new List<IBoard> { board.Object });
            repository.Setup(x => x.GetUserByName(userName)).Returns(user.Object);
            repository.SetupGet(x => x.Teams).Returns(new List<ITeam> { team.Object });
            repository.Setup(x => x.TeamExists(teamName)).Returns(true);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem>());
            board.Setup(x => x.WorkItems).Returns(new List<IWorkItem>());
           
            var test = new CreateStoryOnBoard(id, commandParameters, repository.Object);
            var actual = test.Execute();
            var expected = $"Created story: {title} on board {boardName} in {teamName}";
            Assert.AreEqual(actual, expected);
        }
    }
}

