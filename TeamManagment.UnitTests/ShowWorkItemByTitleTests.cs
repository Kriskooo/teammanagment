﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagement.Commands.ShowCommands;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ShowWorkItemByTitleTests
    {
        [TestMethod]
        public void ShouldThrow_IfParametersLessThanOne()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string>();

            Assert.ThrowsException<ArgumentException>(() => new ShowWorkItemByTitle(commandParameters, repository.Object).Execute());
        }

        [TestMethod]
        public void ShouldPrint_Ascending()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string> { "Ascending" };

            var itemOne = new Mock<IBug>();
            itemOne.SetupGet(x => x.Title).Returns("Abc");
            var itemTwo = new Mock<IBug>();
            itemTwo.SetupGet(x => x.Title).Returns("Bcd");
            var itemThree = new Mock<IBug>();
            itemThree.SetupGet(x => x.Title).Returns("Cde");
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { itemOne.Object, itemTwo.Object, itemThree.Object });
            var list = repository.Object.WorkItems.OrderBy(x => x.Title);
            var test = new ShowWorkItemByTitle(commandParameters, repository.Object);
            var actual = test.Execute();
            var sb = new StringBuilder();
            foreach (var item in list)
            {
                sb.AppendLine(item.Title);
            }             
            var expected = sb.ToString();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ShouldPrint_Descending()
        {
            var repository = new Mock<IRepository>();
            var commandParameters = new List<string> { "Descending" };

            var itemOne = new Mock<IBug>();
            itemOne.SetupGet(x => x.Title).Returns("Abc");
            var itemTwo = new Mock<IBug>();
            itemTwo.SetupGet(x => x.Title).Returns("Bcd");
            var itemThree = new Mock<IBug>();
            itemThree.SetupGet(x => x.Title).Returns("Cde");
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { itemOne.Object, itemTwo.Object, itemThree.Object });
            var list = repository.Object.WorkItems.OrderByDescending(x => x.Title);
            var test = new ShowWorkItemByTitle(commandParameters, repository.Object);
            var actual = test.Execute();
            var sb = new StringBuilder();
            foreach (var item in list)
            {
                sb.AppendLine(item.Title);
            }
            var expected = sb.ToString();

            Assert.AreEqual(expected, actual);
        }
    }
}
