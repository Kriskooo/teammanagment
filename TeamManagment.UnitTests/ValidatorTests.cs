﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Common;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ValidatorTests
    {
        [TestMethod]
        public void Should_Throw_WhenNull()
        {
            Assert.ThrowsException<ArgumentException>(() => Validator.ValidateArgumentIsNotNull(null, Constants.ADD_NULL_ERR));
        }

        [TestMethod]
        public void Should_Throw_WhenParametersMin()
        {
            Assert.ThrowsException<ArgumentException>(() => Validator.CreateMinParameters(3, 5, Constants.CREATE_TEAM_LEN_ERR));
        }
    }
}
