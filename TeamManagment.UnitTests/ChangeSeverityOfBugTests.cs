﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TaskManagement.Commands;
using TaskManagement.Commands.Creating;
using TaskManagement.Contracts;
using TaskManagement.Core.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class ChangeSeverityOfBugTests
    {
        [TestMethod]
        public void Execute_Should_ChangePriority()
        {
            var itemName = "Test Title";
            var severity = "MAJOR";
            var previosPriority = Severity.CRITICAL;
            var commandParameters = new List<string> { itemName, severity };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IBug>();

            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem> { testItem.Object });
            var test = new ChangeSeverityOfBug(commandParameters, repository.Object);
            var actual = test.Execute();
            var expected = $"Changed severity of Bug {itemName} with ID: {testItem.Object.Id} from {previosPriority} to {severity}";
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void Execute_ShouldThrow_IfWorkItemDoesNotExists()
        {
            var itemName = "Test Title";
            var severity = "MAJOR";
            //var previosPriority = Severity.CRITICAL;
            var commandParameters = new List<string> { itemName, severity };
            var repository = new Mock<IRepository>();
            var testItem = new Mock<IBug>();
            testItem.SetupGet(x => x.Severity).Returns(Severity.CRITICAL);
            testItem.SetupGet(x => x.Title).Returns(itemName);
            testItem.SetupGet(x => x.Id).Returns(1);
            repository.SetupGet(x => x.WorkItems).Returns(new List<IWorkItem>());
            var test = new ChangeSeverityOfBug(commandParameters, repository.Object);
            Assert.ThrowsException<ArgumentException>(() => test.Execute());
        }
    }
}
