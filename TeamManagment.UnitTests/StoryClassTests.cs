﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagement.Contracts;
using TaskManagement.Enums;
using TaskManagement.Models;

namespace TeamManagment.UnitTests
{
    [TestClass]
    public class StoryClassTests
    {
        [TestMethod]
        public void PassValues_Should()
        {
            //Arrange
            var id = 1;
            var title = "Test Titleee";
            var description = "Test Description";
            var user = new Mock<IUser>();
            user.SetupGet(x => x.Name).Returns("Johnny");


            //Act
            var story = new Story(id, title, description, Priority.HIGH, Size.LARGE, StoryStatus.DONE);
            story.Assignee = user.Object;

            //Assert
            Assert.AreEqual(story.Title, "Test Titleee");
            Assert.AreEqual(story.Description, "Test Description");
            Assert.AreEqual(story.Priority, Priority.HIGH);
            Assert.AreEqual(story.Size, Size.LARGE);
            Assert.AreEqual(story.Status, StoryStatus.DONE);
            Assert.AreEqual("Johnny", story.Assignee.Name);
        }
    }
}
